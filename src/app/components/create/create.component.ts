import { Component, OnInit } from '@angular/core';
import { UserService } from '../../user.service';
import { FormGroup,  FormBuilder,  Validators } from '@angular/forms';

@Component({
  selector: 'app-create',
  templateUrl: './create.component.html',
  styleUrls: ['./create.component.css']
})
export class CreateComponent implements OnInit {

    title = 'add user'
  angForm: FormGroup;
  constructor(private userservice: UserService, private fb: FormBuilder) {
    this.createForm();
   }

  createForm() {
    this.angForm = this.fb.group({
      user_first_name: ['', Validators.required ],
      user_last_name: ['', Validators.required ],
      user_email: ['', Validators.required ],
      user_bd: ['', Validators.required ]
   });
  }
  addUser(user_first_name, user_last_name, user_email, user_bd) {
      this.userservice.addUser(user_first_name, user_last_name, user_email, user_bd);
  }

  ngOnInit() {
  }

}
