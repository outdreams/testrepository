// edit.component.ts

import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { UserService } from './../../user.service';
import { FormGroup,  FormBuilder,  Validators } from '@angular/forms';

@Component({
  selector: 'app-edit',
  templateUrl: './edit.component.html',
  styleUrls: ['./edit.component.css']
})
export class EditComponent implements OnInit {

  user: any;
  angForm: FormGroup;
  title = 'Edit User';
  constructor(private route: ActivatedRoute, private router: Router, private service: UserService, private fb: FormBuilder) {
    this.createForm();
   }

  createForm() {
    this.angForm = this.fb.group({
      user_first_name: ['', Validators.required ],
      user_last_name: ['', Validators.required ],
      user_email: ['', Validators.required ],
      user_bd: ['', Validators.required ]
   });
//    this.angForm.get('user_first_name').setValue(this.user.name);
console.log(this.user);
  }

  updateUser( user_first_name,user_last_name, user_email, user_bd ) {
    this.route.params.subscribe(params => {
    this.service.updateUser(user_first_name,user_last_name, user_email, user_bd, params['id']);
    this.router.navigate(['index']);
  });
}

  ngOnInit() {
    this.route.params.subscribe(params => {
       
      this.service.editUser(params['id']).subscribe(res => {
        this.user = res;
      });
      console.log(this.user);
    });

  }
}
