import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { FormGroup,  FormBuilder,  Validators } from '@angular/forms';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class UserService {

  constructor(private http: HttpClient) {}
  addUser(user_first_name, user_last_name, user_email, user_bd) {
    const uri = 'https://api.mlab.com/api/1/databases/test_project_db/collections/Users?apiKey=wFQB8CdYAoPUfDa3y7_0NOO-q0jg6cmQ';
    const obj = {
      name: user_first_name,
      lastName: user_last_name,
      email: user_email, 
      bd: user_bd
    };
    this.http.post(uri, obj)
        .subscribe(res => console.log('Done'));
  }

  getUsers() {
    const uri = 'https://api.mlab.com/api/1/databases/test_project_db/collections/Users?apiKey=wFQB8CdYAoPUfDa3y7_0NOO-q0jg6cmQ';
    return this
            .http
            .get(uri)
            .pipe(map(res => {
              return res;
            }));
  }
    editUser(id) {
    const uri = 'https://api.mlab.com/api/1/databases/test_project_db/collections/' + id + 'Users?apiKey=wFQB8CdYAoPUfDa3y7_0NOO-q0jg6cmQ';
    return this
            .http
            .get(uri)
            .pipe(map(res => {
              return res;
            }));
  }

  updateUser(user_first_name,user_last_name, user_email, user_bd, id) {
    const uri = 'https://api.mlab.com/api/1/databases/test_project_db/collections/' + id + 'Users?apiKey=wFQB8CdYAoPUfDa3y7_0NOO-q0jg6cmQ';

    const obj = {
      name: user_first_name,
      lastName: user_last_name,
      email: user_email, 
      bd: user_bd
    };
    this
      .http
      .put(uri, obj);
  }

  deleteUser(id) {
    const uri = 'https://api.mlab.com/api/1/databases/test_project_db/collections/' + id + 'Users?apiKey=wFQB8CdYAoPUfDa3y7_0NOO-q0jg6cmQ';

        return this
            .http
            .delete(uri);
  }

 }